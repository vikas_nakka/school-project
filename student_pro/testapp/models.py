from django.db import models
# Create your models here.

class Grade(models.Model):
    grade = models.CharField(primary_key=True,max_length=50)

    def __str__(self):
        return self.grade

class Section(models.Model):
    grades = models.ForeignKey('Grade',on_delete=models.CASCADE)
    sec = models.CharField(max_length=50,default='Enter')

    def __str__(self):
        return self.sec

class Subject(models.Model):
    grade = models.ForeignKey('Grade',on_delete=models.CASCADE)
    section = models.ForeignKey('Section',on_delete=models.CASCADE)
    subject = models.CharField(max_length=50,default='Enter')

    def __str__(self):
        return self.subject

#
class student(models.Model):
    name=models.CharField(max_length=30)
    stu_id=models.IntegerField()
    grade=models.ForeignKey(Grade,on_delete=models.CASCADE)
    Section=models.ForeignKey(Section,on_delete=models.CASCADE)
    email=models.EmailField(max_length=30,null=True,unique=True,blank=True)
    phone=models.CharField(max_lenght=10,null=True,blank=True,unique=True)
    pro_pic=models.ImageField(upload_to=None,blank=True)

    def __str__(self):
        return self.name+"-"+self.stu_id+"-"+self.grade+"-"+self.Section+"-"+self.email+"-"+self.phone+"-"+self.pro_pic

