from django.contrib import admin
from .models import Grade,Section,Subject
# Register your models here.


class GradeAdmin(admin.ModelAdmin):
    list_display=['grade']

class SectionAdmin(admin.ModelAdmin):
    list_display=['grades','sec']

class SubjectAdmin(admin.ModelAdmin):
    list_display=['grade','section','subject']





admin.site.register(Grade,GradeAdmin)
admin.site.register(Section,SectionAdmin)
admin.site.register(Subject,SubjectAdmin)
