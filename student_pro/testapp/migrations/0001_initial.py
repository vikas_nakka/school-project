# Generated by Django 3.1.3 on 2020-11-11 06:37

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Grade',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gradeA', models.IntegerField()),
                ('gradeB', models.IntegerField()),
                ('gradeC', models.IntegerField()),
                ('gradeD', models.IntegerField()),
                ('gradeE', models.IntegerField()),
                ('gradeF', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Student_class',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('class1', models.IntegerField()),
                ('class2', models.IntegerField()),
                ('class3', models.IntegerField()),
                ('class4', models.IntegerField()),
                ('class5', models.IntegerField()),
                ('class6', models.IntegerField()),
                ('class7', models.IntegerField()),
                ('class8', models.IntegerField()),
                ('class9', models.IntegerField()),
                ('class10', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='subjects',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sub1', models.CharField(max_length=30)),
                ('sub2', models.CharField(max_length=30)),
                ('sub3', models.CharField(max_length=30)),
                ('sub4', models.CharField(max_length=30)),
                ('sub5', models.CharField(max_length=30)),
            ],
        ),
    ]
